package es.com.inetum.repository;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import es.com.inetum.model.Usuario;

@SpringBootTest
class IUsuarioRepositoryTest {

	@Autowired
	IUsuarioRepository usuarioRepository;
	Usuario usuario;
	
	@BeforeEach
	void setUp() throws Exception {
		usuario = new Usuario (0,"David","david");
	}
	
	@AfterEach
	void tearDown() throws Exception {
		usuario = null;
		
	}
	@Test
	void testAgregar() {
		Usuario uruarioRetorno = usuarioRepository.save(usuario);
		assertEquals("david", uruarioRetorno.getClave());
	}

}
