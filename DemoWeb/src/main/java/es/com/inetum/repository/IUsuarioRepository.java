package es.com.inetum.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import es.com.inetum.model.Usuario;

public interface IUsuarioRepository extends JpaRepository<Usuario, Integer> {

	public Usuario findByNombre(String username);

}
