package es.com.inetum.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import es.com.inetum.model.Persona;

public interface IPersonaRepository extends JpaRepository<Persona, Integer> {

}
