package es.com.inetum.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.com.inetum.model.Persona;
import es.com.inetum.repository.IPersonaRepository;

@RestController
@RequestMapping("/personas")
public class RestDemoController {

	@Autowired
	private IPersonaRepository personaRepository;
	
	@GetMapping
	public List<Persona> listar(){
		return personaRepository.findAll();
	}
	
	@PostMapping
	public void insertar(Persona persona) {
		personaRepository.save(persona);
	}
	
}
