package es.com.inetum.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import es.com.inetum.repository.IPersonaRepository;

@Service
public class PersonaServiceImpl implements IPersonaService {
	
	@Autowired
	@Qualifier("persona2")
	IPersonaRepository personaRepository;
	
	@Override
	public void registrarHandler(String nombre) {
		personaRepository.registrar(nombre);
	}

}
