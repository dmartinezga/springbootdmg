package es.com.inetum.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier("persona1")
public class PersonaRepositoryImpl1 implements IPersonaRepository {

	private static Logger log = LoggerFactory.getLogger(PersonaRepositoryImpl1.class);

	@Override
	public void registrar(String nombre) {
		log.info("Se registró el usuario: " + nombre);
	}

}
