package es.com.inetum.elementos.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Esta clase resuelve el DriverManager y la conexión a base de datos
 * @author david
 *
 */
public class ConnectionManager {
	
	private static Connection conexion;
	
	public ConnectionManager() {}
	
	public static void conectar() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/inetum","inetum","inetum");
	}
	
	public static Connection getConexion() {
		return conexion;
	}
	
	public static void desconectar() throws SQLException {
		conexion.close();
	}

}
