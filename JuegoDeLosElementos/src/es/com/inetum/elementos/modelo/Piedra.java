package es.com.inetum.elementos.modelo;

public class Piedra extends ElementoFactory {

	public Piedra() {
		super("piedra",0);
	}

	@Override
	public int comparar(ElementoFactory param) {

		switch (param.getNumber())
		{
			case TIJERA: {
				descripcionResultado = "Piedra gana a Tijera";
				return 1;
			}
			case PAPEL: {
				descripcionResultado = "Piedra pierde con Papel";
				return 2;
			}
			default: {
				descripcionResultado = "Empate";
				return 0;
			}
		}
	}

}
