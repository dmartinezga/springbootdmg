package es.com.inetum.elementos.modelo;

import java.util.ArrayList;
import java.util.List;

public abstract class ElementoFactory {

	public static final int PIEDRA = 0;
	public static final int PAPEL = 1;
	public static final int TIJERA = 2;

	protected String descripcionResultado;
	private static List<ElementoFactory> elementos;
	protected String nombre;
	protected int numero;

	public ElementoFactory(String nombre,int numero) {
		this.nombre = nombre;
		this.numero = numero;
	}

	public String getDescripcionResultado() {
		return descripcionResultado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumber() {
		return numero;
	}

	public void setNumero(int number) {
		this.numero = number;
	}

	public boolean isMe(int param) {
		return numero==param;
	}

	public abstract int comparar(ElementoFactory param);

	public static ElementoFactory getInstance(int param) {
		elementos = new ArrayList<>();

		elementos.add(new Piedra());
		elementos.add(new Papel());
		elementos.add(new Tijera());

		for (ElementoFactory elementoFactory : elementos) {
			if(elementoFactory.isMe(param))
				return elementoFactory;
		}
		return null;
	}

}
