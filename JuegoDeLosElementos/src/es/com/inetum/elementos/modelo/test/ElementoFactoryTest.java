package es.com.inetum.elementos.modelo.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import es.com.inetum.elementos.modelo.ElementoFactory;
import es.com.inetum.elementos.modelo.Papel;
import es.com.inetum.elementos.modelo.Piedra;
import es.com.inetum.elementos.modelo.Tijera;


public class ElementoFactoryTest {
	//lote de pruebas
	Piedra piedra;
	Papel papel;
	Tijera tijera;

	@Before
	public void setUp() throws Exception {
		//antes de cada testeo
		piedra = new Piedra();
		papel = new Papel();
		tijera = new Tijera();
	}

	@After
	public void tearDown() throws Exception {
		//después de cada testeo
		piedra = null;
		papel = null;
		tijera = null;
	}

	@Test
	public void testCompararPiedraConTijera() {
		assertEquals(1, piedra.comparar(tijera));		
		assertEquals("Piedra le ganó a Tijera", piedra.getDescripcionResultado());		
	}

	@Test
	public void testCompararPiedraConPapel() {
		assertEquals(-1, piedra.comparar(papel));
		assertEquals("Piedra perdió con Papel", piedra.getDescripcionResultado());
	}
	
	@Test
	public void testCompararPiedraConPiedra() {
		assertEquals(0, piedra.comparar(piedra));
		assertEquals("Empate", piedra.getDescripcionResultado());
	}
	
	@Test
	public void testCompararPapelConTijera() {
		assertEquals(-1, papel.comparar(tijera));
		assertEquals("Papel perdió con Tijera", papel.getDescripcionResultado());
	}

	@Test
	public void testCompararPapelConPapel() {
		assertEquals(0, papel.comparar(papel));
		assertEquals("Empate", papel.getDescripcionResultado());
	}
	
	@Test
	public void testCompararPapelConPiedra() {
		assertEquals(1, papel.comparar(piedra));
		assertEquals("Papel le ganó a Piedra", papel.getDescripcionResultado());
	}
	
	@Test
	public void testCompararTijeraConTijera() {
		assertEquals(0, tijera.comparar(tijera));
		assertEquals("Empate", tijera.getDescripcionResultado());
	}
	
	@Test
	public void testCompararTijeraConPapel() {
		assertEquals(1, tijera.comparar(papel));
		assertEquals("Tijera le ganó a Papel", tijera.getDescripcionResultado());
	}
	
	@Test
	public void testCompararTijeraConPiedra() {
		assertEquals(-1, tijera.comparar(piedra));
		assertEquals("Tijera perdió con Piedra", tijera.getDescripcionResultado());
	}
	
	@Test
	public void testGetInstancePiedra() {
		assertTrue(ElementoFactory.getInstance(0) instanceof Piedra);
	}
	
	@Test
	public void testGetInstancePapel() {
		assertTrue(ElementoFactory.getInstance(1) instanceof Papel);
	}
	
	@Test
	public void testGetInstanceTijera() {
		assertTrue(ElementoFactory.getInstance(2) instanceof Tijera);
	}

}