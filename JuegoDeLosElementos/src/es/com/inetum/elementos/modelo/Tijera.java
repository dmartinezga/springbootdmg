package es.com.inetum.elementos.modelo;

public class Tijera extends ElementoFactory {

	public Tijera() {
		super("tijera",2);
	}

	@Override
	public int comparar(ElementoFactory param) {

		switch (param.getNumber())
		{
			case PAPEL: {
				descripcionResultado = "Tijera gana a Papel";
				return 1;
			}
			case PIEDRA: {
				descripcionResultado = "Tijera pierde con Piedra";
				return 2;
			}
			default: {
				descripcionResultado = "Empate";
				return 0;
			}
		}
	}

}
