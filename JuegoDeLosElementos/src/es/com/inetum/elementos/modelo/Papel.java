package es.com.inetum.elementos.modelo;

public class Papel extends ElementoFactory {

	public Papel() {
		super("papel",1);
	}

	@Override
	public int comparar(ElementoFactory param) {

		switch (param.getNumber())
		{
			case PIEDRA: {
				descripcionResultado = "Papel gana a Piedra";
				return 1;
			}
			case TIJERA: {
				descripcionResultado = "Papel pierde con Tijera";
				return 2;
			}
			default: {
				descripcionResultado = "Empate";
				return 0;
			}
		}
	}

}
