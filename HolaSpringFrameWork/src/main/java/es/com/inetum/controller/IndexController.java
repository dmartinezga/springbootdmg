package es.com.inetum.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
	
	@RequestMapping("/home")
	public String goIndex() {
		return "index";
	}
	
	@RequestMapping("/")
	public String getPresentacion() {
		return "presentacion";
	}
	
	@RequestMapping("/listado")
	public String goListado(Model model) {
		List<String> alumnos = new ArrayList<String>();
		alumnos.add("David");
		alumnos.add("Nuria");
		alumnos.add("Ivan");
		alumnos.add("Rubén");
		
		model.addAttribute("titulo", "Listado de alumnos");
		model.addAttribute("profesor", "Gabriel Casas");
		model.addAttribute("alumnos", alumnos);
		
		return "listado";
	}

}
