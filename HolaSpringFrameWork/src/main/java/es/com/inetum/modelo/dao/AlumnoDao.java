package es.com.inetum.modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import es.com.inetum.modelo.Alumno;
import es.com.inetum.modelo.IModel;
import es.com.inetum.util.ConnectionManager;

public class AlumnoDao implements IDao {

	private List<IModel> listaUsuarios;

	public AlumnoDao() {}

	@Override
	public void agregar(IModel model) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConexion();
		
		try {		
			StringBuilder sql = new StringBuilder("insert into alumnos(alu_nombre, alu_apellido, alu_email, alu_conocimientos, alu_git, alu_observaciones)");
			sql.append("values(?,?,?,?,?,?)");
			
			PreparedStatement ps = con.prepareStatement(sql.toString());
			
			Alumno alumno = (Alumno) model;
			ps.setString(1, alumno.getNombre());
			ps.setString(2, alumno.getApellido());
			ps.setString(3, alumno.getEmail());
			ps.setString(4, alumno.getConocimientos());
			ps.setString(5, alumno.getGit());
			ps.setString(6, alumno.getObservaciones());
			
			ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
            if (con != null) {
                con.close();
            }
        }
		
		ConnectionManager.desconectar();
	}

	@Override
	public void modificar(IModel model) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConexion();
		
		try {		
			StringBuilder sql = new StringBuilder("update alumnos set alu_nombre=?, apellido=?, email=?, conocimientos=?, git=?, observaciones=? where codigo=?");
			
			PreparedStatement ps = con.prepareStatement(sql.toString());
			
			Alumno alumno = (Alumno) model;
			ps.setString(1, alumno.getNombre());
			ps.setString(2, alumno.getApellido());
			ps.setString(3, alumno.getEmail());
			ps.setString(4, alumno.getConocimientos());
			ps.setString(5, alumno.getGit());
			ps.setString(6, alumno.getObservaciones());
			ps.setInt(7, alumno.getCodigo());
			
			ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
            if (con != null) {
                con.close();
            }
        }
		
		ConnectionManager.desconectar();
	}

	@Override
	public void eliminsar(IModel model) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConexion();
		
		try {		
			StringBuilder sql = new StringBuilder("delete alumnos where codigo=?");
			
			PreparedStatement ps = con.prepareStatement(sql.toString());
			
			Alumno alumno = (Alumno) model;
			ps.setInt(1, alumno.getCodigo());
			
			ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
            if (con != null) {
                con.close();
            }
        }
		
		ConnectionManager.desconectar();
	}

	@Override
	public List<IModel> leer(IModel model) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConexion();
		
		listaUsuarios = null;
		
		try {	
			StringBuilder sql = new StringBuilder("select codigo,nombre,apellido,email,conocimienos,git,observaciones from usuarios");
			Statement st = con.createStatement();
			ResultSet result = st.executeQuery(sql.toString());
			 
			int count = 0;
			 
			while (result.next()){
			    int codigo = result.getInt("codigo");
			    String nombre = result.getString("nombre");
			    String apellido = result.getString("apellido");
			    String email = result.getString("email");
			    String conocimientos = result.getString("conocimientos");
			    String git = result.getString("git");
			    String observaciones = result.getString("observaciones");
			    
			    Alumno alumno = new Alumno(codigo,nombre,apellido,email,conocimientos,git,observaciones);
			    
			    listaUsuarios.add((IModel)alumno);
			    
			    String output = "User #%d: %s - %s - %s - %s- %s- %s- %s";
			    System.out.println(String.format(output, ++count, codigo, nombre, apellido, email, conocimientos, git, observaciones));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
            if (con != null) {
                con.close();
            }
        }
		
		return listaUsuarios;
	}

}
