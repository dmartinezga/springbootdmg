package es.com.inetum.modelo;

import java.util.Date;
import java.util.Objects;

public class Practica implements IModel, IVaciable {

	private int codigo;
	private String nombre;
	private Date fecha;
	private String resultado;
	private String observaciones;
	private String codigoAlumno;
	
	public Practica() {};
	
	public Practica(int codigo, String nombre, Date fecha, String resultado, String observaciones,String codigoAlumno) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.fecha = fecha;
		this.resultado = resultado;
		this.observaciones = observaciones;
		this.codigoAlumno = codigoAlumno;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getCodigoAlumno() {
		return codigoAlumno;
	}

	public void setCodigoAlumno(String codigoAlumno) {
		this.codigoAlumno = codigoAlumno;
	}

	@Override
	public boolean isEmpty() {
		return codigo == 0 && 
			   (nombre == null || nombre.isEmpty()) && 
			   (fecha == null || fecha.toString().isEmpty()) &&
			   (resultado == null || resultado.isEmpty()) &&
			   (observaciones == null || observaciones.isEmpty()) &&
			   (codigoAlumno == null || codigoAlumno.isEmpty());
	}   

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("codigo=");
		sb.append(codigo);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", fecha=");
		sb.append(fecha);
		sb.append(", resultado=");
		sb.append(resultado);
		sb.append(", observaciones=");
		sb.append(observaciones);
		sb.append(", codigoAlumno=");
		sb.append(codigoAlumno);
		
		return sb.toString();
	}

	@Override
	public int hashCode() {
		return Objects.hash(codigo, codigoAlumno, fecha, nombre);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Practica other = (Practica) obj;
		return codigo == other.codigo && Objects.equals(codigoAlumno, other.codigoAlumno)
				&& Objects.equals(fecha, other.fecha) && Objects.equals(nombre, other.nombre);
	}
		
}
