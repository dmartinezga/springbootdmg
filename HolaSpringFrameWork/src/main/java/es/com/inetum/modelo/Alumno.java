package es.com.inetum.modelo;

import java.util.Objects;

public class Alumno implements IModel, IVaciable {

	private int codigo;
	private String nombre;
	private String apellido;
	private String email;
	private String conocimientos;
	private String git;
	private String observaciones;
	
	public Alumno() {};
	
	public Alumno(int codigo, String nombre, String apellido, String email, String conocimientos, String git,
			String observaciones) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.conocimientos = conocimientos;
		this.git = git;
		this.observaciones = observaciones;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getConocimientos() {
		return conocimientos;
	}

	public void setConocimientos(String conocimientos) {
		this.conocimientos = conocimientos;
	}

	public String getGit() {
		return git;
	}

	public void setGit(String git) {
		this.git = git;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Override
	public boolean isEmpty() {
		return codigo == 0 && 
			   (nombre == null || nombre.isEmpty()) && 
			   (apellido == null ||apellido.isEmpty()) &&
			   (email == null || email.isEmpty()) &&
			   (conocimientos == null || conocimientos.isEmpty()) &&
			   (git == null || git.isEmpty()) &&
			   (observaciones == null || observaciones.isEmpty());
	}   

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("codigo=");
		sb.append(codigo);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", apellido=");
		sb.append(apellido);
		sb.append(", email=");
		sb.append(email);
		sb.append(", conocimientos=");
		sb.append(conocimientos);
		sb.append(", git=");
		sb.append(git);
		sb.append(", observaciones=");
		sb.append(observaciones);
		
		return sb.toString();
	}

	@Override
	public int hashCode() {
		return Objects.hash(apellido, codigo, email, nombre);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alumno other = (Alumno) obj;
		return Objects.equals(apellido, other.apellido) && codigo == other.codigo && Objects.equals(email, other.email)
				&& Objects.equals(nombre, other.nombre);
	}
		
}
