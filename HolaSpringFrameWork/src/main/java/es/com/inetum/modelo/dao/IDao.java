package es.com.inetum.modelo.dao;

import java.sql.SQLException;
import java.util.List;

import es.com.inetum.modelo.IModel;

public interface IDao {

	public void agregar(IModel model) throws ClassNotFoundException, SQLException;
	
	public void modificar(IModel model) throws ClassNotFoundException, SQLException;
	
	public void eliminsar(IModel model) throws ClassNotFoundException, SQLException;
	
	public List<IModel> leer(IModel model) throws ClassNotFoundException, SQLException;
}
