<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<title>${titulo}</title>
		<h1>PROFESOR: ${profesor}</h1>
		</br>
	</head>
	
	<body>
		<ul>
			<c:forEach items="${alumnos}" var="alumno">
				<li><c:out value="${alumno}" /></li>
			</c:forEach>
		</ul>	
	</body>
	
</html>